package execjob

import "testing"

func IsValidSignatureTest(t *testing.T) {
	secret := "A Secret"
	job := Job{
		JobName: "Foobar",
		JobID:   "asdkfj3il3j2kj",
		Env: map[string]string{
			"Foo": "Bar",
		},
	}

	signedJob, err := SignJob(secret, job)
	if err != nil {
		t.Error(err)
	}

	isValid, err := IsValidSignature(secret, *signedJob)
	if err != nil {
		t.Error(err)
	}

	if !isValid {
		t.Error("Expected to be valid")
	}

	isValid, err = IsValidSignature(secret+"d", *signedJob)

	if err != nil {
		t.Error(err)
	}

	if isValid {
		t.Error("Expected to be invalid")
	}
}
