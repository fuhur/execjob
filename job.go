package execjob

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"

	"gitlab.com/fuhur/execconf"
)

// Job a struct to represent a job in json
type Job struct {
	JobName     string              `json:"jobName"`
	JobID       string              `json:"jobId"`
	ExecConfigs []execconf.ExecConf `json:"execConfigs"`
	Env         map[string]string   `json:"ENV"`
}

// SignedJob represents a job with a signature
type SignedJob struct {
	Job       Job    `json:"job"`
	Signature string `json:"signature"`
}

func signData(secret, data []byte) []byte {
	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(secret))
	// Write Data to it
	h.Write(data)

	return h.Sum(nil)
}

// SignJob signs the job passed using the secret and returns the job with the secret
func SignJob(secret string, job Job) (*SignedJob, error) {
	b, err := json.Marshal(job)

	if err != nil {
		return nil, err
	}

	h := signData([]byte(secret), b)

	signedJob := &SignedJob{
		Job:       job,
		Signature: hex.EncodeToString(h),
	}
	return signedJob, nil
}

// IsValidSignature Checkf is signed job signature is valid with given secret
func IsValidSignature(secret string, signedJob SignedJob) (bool, error) {
	origiSignature, err := hex.DecodeString(signedJob.Signature)

	if err != nil {
		return false, err
	}

	b, err := json.Marshal(signedJob.Job)

	if err != nil {
		return false, err
	}

	calculatedSignature := signData([]byte(secret), []byte(b))

	return hmac.Equal(origiSignature, calculatedSignature), nil
}
